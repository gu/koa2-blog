
const Router = require('koa-router');
const common = require('../libs/common');

let router = new Router();

router.get('/list', async ctx => {
    // let data = await ctx.db.query('select c.id, c.name, c.`key`, c.created_at, count(a.category_id) as category_sum from category c left join article a on c.id = a.category_id group by c.id order by c.created_at desc');
    let data = '<h1>Hello koa2!</h1>'
    ctx.body = common.handleResulte(200, data, '获取分类列表成功')
})

router.use('/')

module.exports = router.routes();
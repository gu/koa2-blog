const Koa = require('koa');
const Router = require('koa-router');
const cors = require('koa2-cors');
const static = require('koa-static');
const body = require('koa-better-body');
const path = require('path');
const config = require('./config/config');
const db = require('./config/database');
const common = require('./app/libs/common');

const server = new Koa();

server.context.db = db;
server.context.config = config;

server.use(cors({
    origin: function(ctx) {
        return '*'
    },
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
    maxAge: 5,
    credentials: true,
    allowMethods: ['GET', 'POST', 'DELETE', 'OPTIONS', 'PUT'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept']
}))

let router = new Router();
router.use(async (ctx, next) => {
    if(ctx.request.url.includes('login') || ctx.request.url.includes('register')) {
        console.log(2)
        await next();
    } else {
        let token = ctx.request.header.authorization;
        await next();
    }
})

// router.use('/api/admin', require(''))
router.use('/api/category', require('./app/api/category'))

router.get('/', async (ctx, next) => {
    ctx.response.body = '<h5>Index</h5>';
})

server.use(router.routes());

server.listen(config.port, () => {
    console.log('This server is running at http://localhost:' + config.port);
});
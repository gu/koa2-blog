const env = process.env.NODE_ENV;

// 开发环境数据库配置
const devConfig = {
    dbName: 'fgblog',
    dbPass: '123456',
    dbHost: 'localhost',
    dbUser: 'root',
    dbPort: 3306,
    timezone: '08:00',  // 处理使用node查询数据库（mysql）时，日期格式不对的问题
    host: 'http://localhost'
}

// 线上环境数据库配置
const protConfig = {
    dbName: 'yangblog',
    dbPass: '',
    dbHost: '',
    dbUser: '',
    dbPort: 3306,
    timezone: '08:00',
    host: ''
}

let config = {}

if(env === 'development') {
    config = devConfig
} else if(env === 'production') {
    config = protConfig
}

module.exports = {
    ...config,
    port: 3001
}